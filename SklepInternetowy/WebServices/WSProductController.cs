﻿
using SklepInternetowy.Database;
using SklepInternetowy.Models;
using SklepInternetowy.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SklepInternetowy.WebServices
{
    public class WSProductController : ApiController
    {
        [HttpDelete]
        public bool DeleteProduct(long productId)
        {

            bool isDeleted = new ProductRepository().DeleteProduct(productId);
            return isDeleted;
        }

        [HttpGet]
        public GridResultModel GridGetProducts([FromUri]GridModel model)
        {
            ProductRepository productRepository = new ProductRepository();

            //var product = productRepository.GetById(26);

            //int a = 0;
            //Product p = new Product()
            //{
            //    Name = "p1"
            //};
            //MojaMetoda(a, p);

            return productRepository.GridGetProducts(model);
        }

        //public void MojaMetoda(int zmiennaTypuProstego, Product zmiennaTypuZlozonego)
        //{
        //    zmiennaTypuProstego = 9;
        //    zmiennaTypuZlozonego.Name = "p9";
        //}

        // dodac metode JEDNA zwrcajaca wszystkie produkty + parametry (GET):
        // - ilosc elementow na stronie
        // - nr strony
        // - tekst po ktorym szukamy
        // - nr kolumny (liczac od 0) po ktorej sortujemy
        // - telst z kierunkiem sortowania ("ASC", "DESC")
        // LINQ take, skip, orderby, orderbydesc, where, 
    }
}
