﻿using SklepInternetowy.Models;
using SklepInternetowy.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SklepInternetowy.WebServices
{
    public class WSUserController : ApiController
    {
        [HttpDelete]
        public bool DeleteUser(long userId)
        {

            bool isDeleted = new UserRepository().DeleteUser(userId);
            return isDeleted;
        }

        [HttpGet]
        public GridResultModel GridGetUsers([FromUri]GridModel model)
        {
            UserRepository userRepository = new UserRepository();

            return userRepository.GridGetUsers(model);
        }
    }
}
