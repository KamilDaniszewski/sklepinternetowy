﻿using SklepInternetowy.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy.Helpers
{
    public static class SessionHelper
    {
        public static long UserId {
            get
            {
                return (long)HttpContext.Current.Session["UserId"];
            }
            set
            {
                HttpContext.Current.Session["UserId"] = value;
            }
        }
        public static string Email
        {
            get
            {
                return (string)HttpContext.Current.Session["Email"];
            }
            set
            {
                HttpContext.Current.Session["Email"] = value;
            }
        }
        public static string Name
        {
            get
            {
                return (string)HttpContext.Current.Session["Name"];
            }
            set
            {
                HttpContext.Current.Session["Name"] = value;
            }
        }
        public static string Surname
        {
            get
            {
                return (string)HttpContext.Current.Session["Surname"];
            }
            set
            {
                HttpContext.Current.Session["Surname"] = value;
            }
        }
    }
}