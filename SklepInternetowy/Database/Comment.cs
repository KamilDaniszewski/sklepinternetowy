//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SklepInternetowy.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Comment
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public long UserId { get; set; }
        public System.DateTime DateOfAddition { get; set; }
        public byte Rate { get; set; }
        public long ProductId { get; set; }
        public bool IsDeleted { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
    }
}
