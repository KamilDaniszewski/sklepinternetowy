﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using SklepInternetowy.Database;
using SklepInternetowy.Enums;
using SklepInternetowy.Helpers;
using SklepInternetowy.Models;
using SklepInternetowy.Repositories;

namespace SklepInternetowy
{
    public class ApplicationUserManager
    {
        public User GetUser(string email, string password)
        {
            //Generowanie hasla do testow.
            //Password newPasswordDeleteThis = PasswordHelper.CreateHash(password);
            User user = new UserRepository().GetByEmailAndPassword(email, password);
            return user;
        }
    }

    public class ApplicationSignInManager
    {
        private IOwinContext OwinContext { get; set; }
        private ApplicationUserManager UserManager { get; set; }
        public ApplicationSignInManager(ApplicationUserManager userManager, IOwinContext owinContext)
        {
            UserManager = userManager;
            OwinContext = owinContext;
        }

        public SignInStatus PasswordSignIn(LoginViewModel model)
        {
            SignInStatus result = SignInStatus.Failure;

            User user = UserManager.GetUser(model.Email, model.Password);
            if (user != null)
            {
                result = SignInStatus.Success;
                var ident = new ClaimsIdentity(
                  new[] {
                      new Claim(ClaimTypes.NameIdentifier, model.Email),
                      new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"),

                      new Claim(ClaimTypes.Name, model.Email),
                      new Claim(ClaimTypes.Role, ((UserRole)user.Role).ToString()),
                  },
                  DefaultAuthenticationTypes.ApplicationCookie);

                OwinContext.Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = false,
                    ExpiresUtc = model.RememberMe ? (DateTime?)DateTime.UtcNow.AddDays(7) : null
                }, ident);
                result = SignInStatus.Success;

                //Session["asdasd"]=asd
                //SessionHelper.LoggedUser = user;
                SessionHelper.UserId = user.Id;
                SessionHelper.Email = user.Email;
                SessionHelper.Name = user.Name;
                SessionHelper.Surname = user.Surname;
            }

            return result;
        }
    }
}
