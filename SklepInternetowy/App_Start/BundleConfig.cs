﻿using System.Web;
using System.Web.Optimization;

namespace SklepInternetowy
{
    public class BundleConfig
    {
        // Aby uzyskać więcej informacji o grupowaniu, odwiedź stronę https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                       // "~/Scripts/bootstrap.js",
                       //"~/Scripts/jquery-{version}.js",
                       //"~/Scripts/respond.js",
                       //"~/Scripts/ShopScripts/jquery.min.js",
                       "~/Scripts/jquery-1.10.2.js",
                       "~/Scripts/jquery.validate.js",
                       "~/Scripts/jquery.validate.unobtrusive.js",
                       "~/Scripts/ShopScripts/bootstrap-3.1.1.min.js",
                       "~/Scripts/ShopScripts/easyResponsiveTabs.js",
                       "~/Scripts/ShopScripts/jquery.flexisel.js",
                       "~/Scripts/ShopScripts/jquery.flexslider.js",
                       "~/Scripts/ShopScripts/minicart.js",
                       "~/Scripts/sweetalert2.all.js",
                       "~/Scripts/ShopScripts/datatables.js",
                       "~/Scripts/autosize.js"
                     ));



            bundles.Add(new StyleBundle("~/Content/css").Include(

                      "~/Content/Site.css",
                      //"~/Content/bootstrap.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/ShopTemplate/style.css",
                      "~/Content/ShopTemplate/fasthover.css",
                      "~/Content/ShopTemplate/popuo-box.css",
                      "~/Content/ShopTemplate/font-awesome.css",
                      "~/Content/ShopTemplate/jquery.countdown.css",
                       "~/Content/ShopTemplate/datatables.css",
                       "~/Content/ShopTemplate/dataTablesl.bootstrap.css"));


            //BundleTable.EnableOptimizations = true;

        }
    }
}
