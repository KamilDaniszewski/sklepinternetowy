﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SklepInternetowy
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void Application_BeginRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current != null && HttpContext.Current.Request.UserLanguages != null)
            {
                string currentLanguage = Request.UserLanguages[0];

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(currentLanguage);
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(currentLanguage);
            }

            //HttpCookie ciasteczko = Request.Cookies["jezyk"];
            //if (ciasteczko != null)
            //{
            //    if (ciasteczko.Value == "en")
            //    {
            //        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            //        Thread.CurrentThread.CurrentUICulture = new CultureInfo("EN");
            //    }
            //}
        }
    }

}
