﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy.Models
{
    public class UserList
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string NumberOfHause{ get; set; }
        public short? ApartmentNumber{ get; set; }
        public string TelephoneNumber{ get; set; }
        public string Country{ get; set; }
        public string City{ get; set; }
        public string Voivodeship{ get; set; }
        public DateTime DateOfAddition{ get; set; }
        public string Role{ get; set; }
        
    }
}