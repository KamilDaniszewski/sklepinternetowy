﻿using SklepInternetowy.Database;
using SklepInternetowy.Enums;
using SklepInternetowy.Repositories;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SklepInternetowy.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Kod")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Pamiętasz tę przeglądarkę?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Adres e-mail")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętać Cię?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        public long Id { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi zawierać co najmniej następującą liczbę znaków: {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Podaj imię")]
        [Display(Name = "Imie")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Podaj nazwisko")]
        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Potwierdź hasło")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Hasło i jego potwierdzenie są niezgodne.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Ulica")]
        public string Street { get; set; }

        //[RegularExpression("/d{2}-/d{3}", ErrorMessage ="Błąd!")]
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }

        [Display(Name = "Numer domu/bloku")]
        public string NumberHause { get; set; }

        [Display(Name = "Numer mieszkania")]
        public short? ApartmentNumber { get; set; }

        [Display(Name = "Numer telefonu")]
        public string TelephoneNumber { get; set; }

        [Display(Name = "Państwo")]
        public string Country { get; set; }

        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Display(Name = "Województwo")]
        public string Voivodeship { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi zawierać co najmniej następującą liczbę znaków: {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potwierdź hasło")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Hasło i jego potwierdzenie są niezgodne.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }
    }

    public class UserViewModel
    {

        public long Id { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Podaj imię")]
        [StringLength(50, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        [Display(Name = "Imie")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Podaj nazwisko")]
        [StringLength(50, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }

        [Display(Name = "Ulica")]
        [StringLength(50, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        public string Street { get; set; }

        [RegularExpression("\\d{2}-\\d{3}", ErrorMessage = "Zły format kodu pocztowego!")]
        [Display(Name = "Kod pocztowy")]
        public string PostalCode { get; set; }

        [Display(Name = "Numer domu/bloku")]
        [StringLength(5, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        public string NumberHause { get; set; }

        [Display(Name = "Numer mieszkania")]
        public short? ApartmentNumber { get; set; }

        [Display(Name = "Numer telefonu")]
        [StringLength(9, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        public string TelephoneNumber { get; set; }

        [Display(Name = "Państwo")]
        [StringLength(50, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        public string Country { get; set; }

        [Display(Name = "Miasto")]
        [StringLength(50, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        public string City { get; set; }

        [Display(Name = "Województwo")]
        [StringLength(50, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        public string Voivodeship { get; set; }
        [Display(Name = "Rola")]
        public byte Role { get; set; }


        public List<SelectListItem> ListOfRole { get; set; }

        public UserViewModel()
        {
            //List<UserRole> listofrole = new List<UserRole>();
            //List<SelectListItem> dropdownlist = new List<SelectListItem>();
            ListOfRole = new List<SelectListItem>();

                SelectListItem element = new SelectListItem();
                element.Text = UserRole.Admin.ToString();
                element.Value = ((long)UserRole.Admin).ToString();
                ListOfRole.Add(element);

            SelectListItem element1 = new SelectListItem();
            element1.Text = UserRole.User.ToString();
            element1.Value = ((long)UserRole.User).ToString();
           ListOfRole.Add(element1);
        }



    }
}
