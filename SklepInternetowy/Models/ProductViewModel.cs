﻿using SklepInternetowy.Database;
using SklepInternetowy.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SklepInternetowy.Models
{
    public class ProductViewModel
    {
        public long? Id { get; set; }
        [Required(ErrorMessage = "Podaj nazwę")]
        [StringLength(100, ErrorMessage = "{0} musi mieścić sie w nastepującej liczbie znaków: {1}.")]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Dodaj opis")]
        [Display(Name = "Opis")]
        public string Description { get; set; }
        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "Podaj cenę")]
        [Display(Name = "Cena")]
        public double Price { get; set; }
        [Required(ErrorMessage = "Wybierz kategorię")]
        [Display(Name = "Kategoria")]
        public long ProductCategoryId { get; set; }
        //public DateTime DateOfAddition { get; set; }


        public List<SelectListItem> ListOfCategory { get; set; }

        public ProductViewModel()
        {
            ProductRepository repo = new ProductRepository();
            List<ProductCategory> listofcategory = repo.GetAllCategories();
            List<SelectListItem> dropdownlist = new List<SelectListItem>();
            foreach (ProductCategory category in listofcategory)
            {
                SelectListItem element = new SelectListItem();
                element.Text = category.Name;
                element.Value = category.Id.ToString();
                dropdownlist.Add(element);
            }

            ListOfCategory = dropdownlist;
        }
    }
}