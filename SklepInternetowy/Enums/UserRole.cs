﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy.Enums
{
    public enum UserRole
    {
        Admin = 1,
        User = 2
    }
}