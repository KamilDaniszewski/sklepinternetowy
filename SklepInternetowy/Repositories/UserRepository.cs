﻿using NLog;
using SklepInternetowy.Database;
using SklepInternetowy.Enums;
using SklepInternetowy.Helpers;
using SklepInternetowy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SklepInternetowy.Repositories
{
    public class UserRepository : RepositoryBase<User>
    {
        public User GetByEmailAndPassword(string email, string password)
        {
            try
            {
                User result = null;

                result = DB.User.Where(p => p.Email == email && p.IsDeleted == false).SingleOrDefault();

                if (result != null)
                {
                    string passwordToHash = password + result.Salt;
                    string hash = MD5Helper.CalculateMD5Hash(passwordToHash);
                    if (hash != result.Password)
                    {
                        result = null;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        public User RegisterNewUser(RegisterViewModel model)
        {
            string salt = Guid.NewGuid().ToString("D");
            string pasAndSalt = model.Password + salt;
            string hashPassword = MD5Helper.CalculateMD5Hash(pasAndSalt);
            try
            {
                User user = new User()
                {
                    Email = model.Email,
                    Salt = salt,
                    Password = hashPassword,
                    Name = model.Name,
                    Surname = model.Surname,
                    DateOfAddition = DateTime.Now,
                    Street = model.Street,
                    PostalCode = model.PostalCode,
                    NumberHause = model.NumberHause,
                    ApartmentNumber = model.ApartmentNumber,
                    TelephoneNumber = model.TelephoneNumber,
                    Country = model.Country,
                    City = model.City,
                    Voivodeship = model.Voivodeship,
                    Role = (byte)UserRole.User
                };
                DB.User.Add(user);
                DB.SaveChanges();
                return user;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        public bool ChangeUserPassword(string name, string newPassword)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAllUsers()
        {
            try
            {
                List<User> users = DB.User.Where(u => u.IsDeleted == false).ToList();
                return users;
            }
            catch
        (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        public GridResultModel GridGetUsers(GridModel model)
        {
            try
            {
                var user1 = DB.User.Where(p => p.IsDeleted == false);
                if (string.IsNullOrEmpty(model.SearchValue) == false)
                {
                    user1 = user1.Where(p => p.Name.Contains(model.SearchValue)
                        || p.Surname.Contains(model.SearchValue) || p.City.Contains(model.SearchValue));
                }
                if (model.OrderDirection.ToUpper() == "ASC")
                {
                    if (model.OrderColumnNo == 0)
                    {
                        user1 = user1.OrderBy(p => p.Id);
                    }
                    else if (model.OrderColumnNo == 2)
                    {
                        user1 = user1.OrderBy(p => p.Name);
                    }
                    else if (model.OrderColumnNo == 3)
                    {
                        user1 = user1.OrderBy(p => p.Surname);
                    }
                    else if (model.OrderColumnNo == 10)
                    {
                        user1 = user1.OrderBy(p => p.City);
                    }

                }
                else
                {
                    if (model.OrderColumnNo == 0)
                    {
                        user1 = user1.OrderByDescending(p => p.Id);
                    }
                    else if (model.OrderColumnNo == 2)
                    {
                        user1 = user1.OrderByDescending(p => p.Name);
                    }
                    else if (model.OrderColumnNo == 3)
                    {
                        user1 = user1.OrderByDescending(p => p.Surname);
                    }
                    else if (model.OrderColumnNo == 10)
                    {
                        user1 = user1.OrderByDescending(p => p.City);
                    }
                }

                user1 = user1.Skip(model.Start);

                GridResultModel resultModel = new GridResultModel(model);
                resultModel.RecordsTotal = user1.Count();
                resultModel.RecordsFiltered = resultModel.RecordsTotal;

                user1 = user1.Take(model.Length);
                //products1 = products1.OrderBy(p => p.Id);
                //products1 = products1.OrderByDescending(p => p.Id);

                List<UserList> listOfUser = new List<UserList>();
                foreach (User user in user1.ToList())
                {
                    listOfUser.Add(new UserList()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Name = user.Name,
                        Surname = user.Surname,
                        Street = user.Street,
                        PostalCode = user.PostalCode,
                        NumberOfHause = user.NumberHause,
                        ApartmentNumber = user.ApartmentNumber,
                        TelephoneNumber = user.TelephoneNumber,
                        Country = user.Country,
                        City = user.City,
                        Voivodeship = user.Voivodeship,
                        DateOfAddition = user.DateOfAddition,
                        Role = ((UserRole)user.Role).ToString()
                    });
                }



                resultModel.Data = listOfUser;
                return resultModel;

            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        public bool DeleteUser(long userId)
        {
            try
            {
                User user = DB.User.Where(p => p.Id == userId).Single();
                user.IsDeleted = true;
                DB.SaveChanges();
                return true;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return false;
            }
        }
        public User GetUserId(long id)
        {
            try
            {
                User user = DB.User.Where(u => u.Id == id).Single();
                return user;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

   

        public User EditUser(UserViewModel model)
        {
            User user = GetById(model.Id); //DB.User.Where(u => u.Id == model.Id).Single();
            user.Email = model.Email;
            user.Name = model.Name;
            user.Surname = model.Surname;
            user.Street = model.Street;
            user.PostalCode = model.PostalCode;
            user.NumberHause = model.NumberHause;
            user.ApartmentNumber = model.ApartmentNumber;
            user.TelephoneNumber= model.TelephoneNumber;
            user.Country= model.Country;
            user.City= model.City;
            user.Voivodeship= model.Voivodeship;
            user.DateOfAddition=DateTime.Now;
            user.Role = model.Role;

            Update(user);
            //DB.SaveChanges();
            return user;

        }
    }
}