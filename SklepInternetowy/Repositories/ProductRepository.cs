﻿using NLog;
using SklepInternetowy.Database;
using SklepInternetowy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SklepInternetowy.Repositories
{
    public class ProductRepository : RepositoryBase<Product>
    {

        public List<ProductCategory> GetAllCategories()
        {
            try
            {
                List<ProductCategory> category = DB.ProductCategory.Where(o => o.IsDeleted == false).ToList();
                return category;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        public Product SaveProducts(ProductViewModel model)
        {
            try
            {
                Product product = null;
                if (model.Id != null)
                {
                    product = DB.Product.Where(p => p.Id == model.Id).Single();
                }
                else
                {
                    product = new Product();
                }
                product.Name = model.Name;
                product.Description = model.Description;
                product.Price = (decimal)model.Price;
                product.ProductCategoryId = model.ProductCategoryId;
                product.DateOfAddition = DateTime.Now;
                if (model.Id == null)
                {
                    DB.Product.Add(product);
                }
                DB.SaveChanges();
                return product;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        public Product GetProductId(long? id)
        {
            try
            {
                Product product = DB.Product.Where(p => p.Id == id).Single();
                return product;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        public List<Product> GetAllProduct()
        {
            try
            {
                List<Product> products = DB.Product.Where(p => p.IsDeleted == false).ToList();
                return products;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

       // public List<Product> GetAllProductDataTables(int quantityOfElements, short numberOfPage, string contents, short numberOfColumn, string orderby)
       public GridResultModel GridGetProducts(GridModel model)
        {
            try
            {
                //var products1 = DB.Product.Where(p => p.IsDeleted == false);
                //if (string.IsNullOrEmpty(model.SearchValue) == false)
                //{
                //    products1 = products1.Where(p => p.Name.Contains(model.SearchValue)
                //    || p.Description.Contains(model.SearchValue) || p.ProductCategory.Name.Contains(model.SearchValue));
                //}
                //if (model.OrderDirection.ToUpper()== "ASC")
                //{
                //    if (model.OrderColumnNo== 0)
                //    {
                //        products1 = products1.OrderBy(p => p.Id);
                //    }
                //    else if (model.OrderColumnNo == 1)
                //    {
                //        products1 = products1.OrderBy(p => p.Name);
                //    }
                //    else if (model.OrderColumnNo == 2)
                //    {
                //        products1 = products1.OrderBy(p => p.Description);
                //    }

                //}
                //else
                //{
                //    if (model.OrderColumnNo== 0)
                //    {
                //        products1 = products1.OrderByDescending(p => p.Id);
                //    }
                //    else if (model.OrderColumnNo == 1)
                //    {
                //        products1 = products1.OrderByDescending(p => p.Name);
                //    }
                //    else if (model.OrderColumnNo == 2)
                //    {
                //        products1 = products1.OrderByDescending(p => p.Description);
                //    }
                //}

                //GridResultModel resultModel = new GridResultModel(model);
                //resultModel.RecordsTotal = products1.Count();
                //resultModel.RecordsFiltered = resultModel.RecordsTotal;

                //products1 = products1.Skip(model.Start);
                //products1 = products1.Take(model.Length);
                ////products1 = products1.OrderBy(p => p.Id);
                ////products1 = products1.OrderByDescending(p => p.Id);

                //List<ProductList> listOfProduct = new List<ProductList>();
                //foreach (Product product in products1.ToList())
                //{
                //    listOfProduct.Add(new ProductList()
                //    {
                //        CategoryName = product.ProductCategory.Name,
                //        Id=product.Id,
                //        Description=product.Description.Replace("\n","<br/>"),
                //        Price=product.Price,
                //        DateOfAddition=product.DateOfAddition,
                //        Name=product.Name
                //    });
                //}

                //resultModel.Data = listOfProduct;

                GridResultModel resultModel = new GridResultModel(model);

                ObjectParameter recordsTotal = new ObjectParameter("RecordsTotal", typeof(int));
                var userList = DB.GridGetProductList(model.SearchValue,model.OrderColumnNo,model.OrderDirection,model.Start,model.Length,recordsTotal).ToList();

                resultModel.RecordsTotal = (int)recordsTotal.Value;
                resultModel.RecordsFiltered = resultModel.RecordsTotal;
                resultModel.Data = userList;
                return resultModel;

                //List<Product> products = DB.Product.Where(p => p.IsDeleted == false)
                //    //.OrderBy()
                //    .ToList();
                //return products;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }


        public bool DeleteProduct(long productId)
        {
            try
            {
                Product product = DB.Product.Where(p => p.Id == productId).Single();
                product.IsDeleted = true;
                DB.SaveChanges();
                return true;
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return false;
            }
        }

    }
}