﻿using NLog;
using SklepInternetowy.Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace SklepInternetowy.Repositories
{
    public class RepositoryBase<T> where T:class
    {
        public SklepInternetowyEntities DB { get; set; }
        public RepositoryBase()
        {
            //List<int> a = new List<int>();
            //a.Add(0);
            //List<string> b = new List<string>();
            //b.Add("asd");
            DB = new SklepInternetowyEntities();
        }

        public T GetById(long id)
        {
            return DB.Set<T>().Find(id);
        }

        public List<T> GetAll()
        {
            return DB.Set<T>().ToList();
        }

        public bool Add(T entity)
        {
            DB.Set<T>().Add(entity);
            DB.SaveChanges();
            return true;
        }

        public bool Update(T entity)
        {
            DB.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            DB.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            try
            {
                T entity = GetById(id);
                bool deleteResult= Delete(entity);
                return deleteResult;

                //DB.Set<T>().Remove(GetById(id));
                //DB.SaveChanges();
                //return true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return false;
            }
        }

        public bool Delete(T entity)
        {
            try
            {
                bool containsIsDeleted = entity.GetType().GetProperties().Where(x => x.Name == "IsDeleted").SingleOrDefault() != null ? true : false;
                bool containsDeleteDate = entity.GetType().GetProperties().Where(x => x.Name == "DeleteDate").SingleOrDefault() != null ? true : false;

                if (containsIsDeleted || containsDeleteDate)
                {
                    if (containsIsDeleted)
                    {
                        entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
                    }
                    if (containsDeleteDate)
                    {
                        entity.GetType().GetProperty("DeleteDate").SetValue(entity, DateTime.Now);
                    }
                    Save(entity);
                }
                else
                {
                    DB.Entry(entity).State = EntityState.Deleted;
                }

                DB.SaveChanges();

                //DB.Set<T>().Remove(entity);
                //DB.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return false;
            }
        }
        //dopisać delete!

        //public virtual bool Delete(TEntity entity)
        //{
        //    try
        //    {
        //        bool result = false;

        //        bool containsIsDeleted = entity.GetType().GetProperties().Where(x => x.Name == "IsDeleted").SingleOrDefault() != null ? true : false;
        //        bool containsDeleteDate = entity.GetType().GetProperties().Where(x => x.Name == "DeleteDate").SingleOrDefault() != null ? true : false;

        //        if (containsIsDeleted || containsDeleteDate)
        //        {
        //            if (containsIsDeleted)
        //            {
        //                entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
        //            }
        //            if (containsDeleteDate)
        //            {
        //                entity.GetType().GetProperty("DeleteDate").SetValue(entity, DateTime.Now);
        //            }
        //            Save(entity);
        //        }
        //        else
        //        {
        //            DBContext.Entry(entity).State = EntityState.Deleted;
        //        }

        //        DBContext.SaveChanges();
        //        result = true;
        //        return result;
        //    }
        //    catch (DbEntityValidationException dbEx)
        //    {
        //        EntityValidationHelper.Log(dbEx);
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.Log.Error(ex);
        //        return false;
        //    }
        //}


// add + update
        public virtual bool Save(T entity)
        {
            try
            {
                bool result = false;
                DB.Entry(entity).State = long.Parse(entity.GetType().GetProperty("Id").GetValue(entity, null).ToString()).Equals(default(long))
                    ? EntityState.Added
                    : EntityState.Modified;
                DB.SaveChanges();
                result = true;
                return result;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return false;
            }
        }



    }

}