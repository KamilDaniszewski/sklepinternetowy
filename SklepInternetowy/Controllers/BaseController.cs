﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SklepInternetowy.Controllers
{
    public class BaseController : Controller
    {
        public void ShowError(string message)
        {
            TempData["error"] = message;
        }
    }
}