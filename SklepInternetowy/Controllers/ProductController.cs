﻿using NLog;
using SklepInternetowy.Database;
using SklepInternetowy.Models;
using SklepInternetowy.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SklepInternetowy.Controllers
{
    public class ProductController : Controller
    {

        //[HttpGet]
        //public ActionResult _AddNewProductPartial()
        //{
        //    ProductViewModel model = new ProductViewModel();
        //    ProductRepository repo = new ProductRepository();
        //    List<ProductCategory> listofcategory = repo.GetAllCategories();
        //    List<SelectListItem> dropdownlist = new List<SelectListItem>();
        //    foreach (ProductCategory category in listofcategory)
        //    {
        //        SelectListItem element = new SelectListItem();
        //        element.Text = category.Name;
        //        element.Value = category.Id.ToString();
        //        dropdownlist.Add(element);
        //        //model.ListOfCategory.Add(element);
        //    }
        //    model.ListOfCategory = dropdownlist;
        //    return View(model);
        //}


        //[HttpPost]
        //public ActionResult AddProduct(ProductViewModel model)
        //{


        //    new ProductRepository().AddProduct(model);
        //    return RedirectToAction("Index", "Home");
        //}

        [HttpGet]
        public ActionResult GetAllProduct()
        {
            try
            {
                var prod = new ProductRepository().GetAllProduct();
                return View(prod);
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        [HttpGet]
        public ActionResult EditProduct(long? id)
        {
            ProductViewModel model = new ProductViewModel();
            try
            {

                if (id != null)
                {
                    ProductRepository repo = new ProductRepository();

                    Product prod = repo.GetProductId(id);
                    model.Id = prod.Id;
                    model.Name = prod.Name;
                    model.Description = prod.Description;
                    model.Price = (double)prod.Price;
                    model.ProductCategoryId = prod.ProductCategoryId;

                }
                return View(model);
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

        [HttpPost]
        public ActionResult SaveProduct(ProductViewModel model)
        {
            try
            {
                new ProductRepository().SaveProducts(model);
                return RedirectToAction("GetAllProduct");
            }
            catch
            (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error(ex);
                return null;
            }
        }

    }
}