﻿using NLog;
using SklepInternetowy.Database;
using SklepInternetowy.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SklepInternetowy.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var logger = LogManager.GetCurrentClassLogger();
            //logger.Error("Hello World");

            //UzytkownikRepository repository = new UzytkownikRepository();
            //Uzytkownik user=repository.GetById(2);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [Authorize(Roles ="Admin")]
        public ActionResult Contact()
        {
           // ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}